-- MySQL dump 10.13  Distrib 8.0.16, for macos10.14 (x86_64)
--
-- Host: 127.0.0.1    Database: employeemgmt
-- ------------------------------------------------------
-- Server version	5.7.25

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
 SET NAMES utf8 ;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `employee`
--

DROP TABLE IF EXISTS `employee`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `employee` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `emp_id` varchar(45) DEFAULT NULL,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `mobile` varchar(45) DEFAULT NULL,
  `city` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=13 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `employee`
--

LOCK TABLES `employee` WRITE;
/*!40000 ALTER TABLE `employee` DISABLE KEYS */;
INSERT INTO `employee` VALUES (11,'3344','subodh edited 2','EMployee','Pune','2017-06-22','2233445566','pune45'),(12,'erterte','eerer','erterter','sdsdfsd','2019-09-05','7777777777','sdsdf');
/*!40000 ALTER TABLE `employee` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `manager`
--

DROP TABLE IF EXISTS `manager`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
 SET character_set_client = utf8mb4 ;
CREATE TABLE `manager` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `first_name` varchar(45) DEFAULT NULL,
  `last_name` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `address` varchar(45) DEFAULT NULL,
  `dob` date DEFAULT NULL,
  `company` varchar(45) DEFAULT NULL,
  `email` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=12 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `manager`
--

LOCK TABLES `manager` WRITE;
/*!40000 ALTER TABLE `manager` DISABLE KEYS */;
INSERT INTO `manager` VALUES (1,'Subodh','Shetyanavar','827CCB0EEA8A706C4C34A16891F84E7B','Pune','2019-09-18','ABC','subodh@gmail.com'),(2,'Subodh 2','Shetyanavar','827CCB0EEA8A706C4C34A16891F84E7B','Pune','2019-09-18','ABC','subodh2@gmail.com'),(3,'Subodh 2','Shetyanavar','827CCB0EEA8A706C4C34A16891F84E7B','Pune','2019-09-18','ABC','subodh3@gmail.com'),(4,'asasas','aasasas','827CCB0EEA8A706C4C34A16891F84E7B','asaasas','2019-09-17','asasasas','aa@aa.com'),(5,'asasdasd','asdasda','827CCB0EEA8A706C4C34A16891F84E7B','asdasda','2019-09-04','asdasas','aa2@aa.com'),(6,'asdasdsd','assdasas','827CCB0EEA8A706C4C34A16891F84E7B','asasasds','2019-09-03','asdasdasdas','pp@pp.com'),(7,'test same email','aasas','827CCB0EEA8A706C4C34A16891F84E7B','asasas','2019-09-02','asasas','subodh9900@gmail.com'),(8,'adasdas','asasdasd','827CCB0EEA8A706C4C34A16891F84E7B','asasas','2019-09-17','asasas','lolo@po.com'),(9,'qwqw','qwqw','827CCB0EEA8A706C4C34A16891F84E7B','qwqw','2019-09-07','qwqwqw','yhyh@yhyh.com'),(10,'qaqa','qaqa','827CCB0EEA8A706C4C34A16891F84E7B','aaaaa','2019-09-10','aaaaaa','zzz@zzz.com'),(11,'subodh','angular','827CCB0EEA8A706C4C34A16891F84E7B','Pune','2019-09-23','Pune.com','subodh9@gmail.com');
/*!40000 ALTER TABLE `manager` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2019-09-22  9:32:58
