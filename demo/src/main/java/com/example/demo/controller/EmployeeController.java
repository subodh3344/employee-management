package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.EmployeeEntity;
import com.example.demo.model.ManagerEntity;
import com.example.demo.services.EmployeeServices;

@RestController
@RequestMapping("/employee")
public class EmployeeController {

	@Autowired
	private EmployeeServices employeeServices;
	
	@GetMapping("/getAllEmployees")
	public ResponseEntity<Object> getAllEmployees(){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			List<EmployeeEntity> optEmployee = employeeServices.getAllEmployee();
			optObject = new ResponseEntity<Object>(optEmployee,HttpStatus.OK);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in getAllEmployees "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in getAllEmployees "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	@PostMapping("/checkEmpIdIfExists")
	public ResponseEntity<Object> checkEmpIdIfExists(@RequestBody EmployeeEntity emp){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return employeeServices.checkEmpIdIfExists(emp);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in checkEmpIdIfExists "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in checkEmpIdIfExists "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	@PostMapping("/createEmployee")
	public ResponseEntity<Object> createEmployee(@RequestBody EmployeeEntity emp){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return employeeServices.createEmployee(emp);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in createEmployee "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in createEmployee "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	@PostMapping("/updateEmployee")
	public ResponseEntity<Object> updateEmployee(@RequestBody EmployeeEntity emp){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return employeeServices.updateEmployee(emp);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in updateEmployee "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in updateEmployee "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	@GetMapping("/deleteEmpById/{empId}")
	public ResponseEntity<Object> deleteEmpById(@PathVariable("empId") Long empId){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return employeeServices.deleteEmpById(empId);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in deleteEmpById "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in deleteEmpById "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
}
