package com.example.demo.controller;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.View;
import org.springframework.web.servlet.view.InternalResourceView;

import java.net.HttpURLConnection;
import java.net.URL;
import java.util.Base64;

/*
 * 
 * Entry point for application 
 * It loads html pages in model */

@RestController
@RequestMapping("/")
public class HomeController {


    static final Logger LOG = LoggerFactory.getLogger(HomeController.class);
    
    @GetMapping(value = "/**/{[path:[^\\.]*}")
    public View redirect() {
        return new InternalResourceView("/");
    }

   @GetMapping("/{page}")
   public ModelAndView index(@PathVariable("page") String page) {


       ModelAndView modelAndView = new ModelAndView();
       try {

           LOG.info("In Home Page ");

           modelAndView.addObject("user", null);
           modelAndView.setViewName(page);
       } catch (Exception e) {
           System.out.println("Exception while loading Home Page. Message :  " + e.getMessage() + " Cause : " + e.getCause());
           LOG.error("Error In Home Page caused by " +e.getCause()  +" message is " + e.getMessage());
       }
       return modelAndView;
   }
   @GetMapping("/")
   public ModelAndView home() {

       ModelAndView modelAndView = new ModelAndView();
       try {
           modelAndView.addObject("user", null);
           modelAndView.setViewName("login");
       } catch (Exception e) {
           System.out.println("Exception while loading / Page. Message :  " + e.getMessage() + " Cause : " + e.getCause());
           LOG.error("Error In / Page caused by " +e.getCause()  +" message is " + e.getMessage());

       }
       return modelAndView;
   }



}