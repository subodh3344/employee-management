package com.example.demo.controller;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.example.demo.model.ManagerEntity;
import com.example.demo.services.ManagerServices;

@RestController
@RequestMapping("/manager")
public class ManagerControllere {

	@Autowired
	private ManagerServices managerServices;
	
	/* Get list of all managers */
	@GetMapping("/getAllManagers")
	public ResponseEntity<Object> getAllManagers(){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			List<ManagerEntity> optManager = managerServices.getAllManagers();
			optObject = new ResponseEntity<Object>(optManager,HttpStatus.OK);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in getAllManagers "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in getAllManagers "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
 
	/* Check manager login 
	 * Input : Email , Password */
	@PostMapping("/checkLogin")
	public ResponseEntity<Object> checkLogin(@RequestBody ManagerEntity manager){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return managerServices.checkLogin(manager);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in checkLogin "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in checkLogin "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	/* Create new manager 
	 * Input : 
	 * Email,First name, Last name , Birth date, Address , Company*/
	@PostMapping("/createManager")
	public ResponseEntity<Object> createManager(@RequestBody ManagerEntity manager){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return managerServices.createManager(manager);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in createManager "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in createManager "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
	
	
	/*Check if email is already present in database 
	 * Input Email
	 * */
	@PostMapping("/checkEmailIfExists")
	public ResponseEntity<Object> checkEmailIfExists(@RequestBody ManagerEntity manager){
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			return managerServices.checkEmailIfExists(manager);
		}
		catch(Exception e) {
			e.printStackTrace();
			System.out.println("Exception in createMcheckEmailIfExistsanager "+e.getMessage());
			optObject = new ResponseEntity<Object>("Exception in checkEmailIfExists "+e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
		return optObject;
	}
}
