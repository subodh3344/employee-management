package com.example.demo.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.EmployeeEntity;
import com.example.demo.model.ManagerEntity;

public interface EmployeeRepositories extends JpaRepository<EmployeeEntity, Long>{

	
	@Query("SELECT e FROM EmployeeEntity e where e.emp_id= :emp_id")
	Optional<EmployeeEntity> findByEmpId(String emp_id);
	
	

}
