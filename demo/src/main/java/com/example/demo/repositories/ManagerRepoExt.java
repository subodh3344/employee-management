package com.example.demo.repositories;

import com.example.demo.model.ManagerEntity;

public interface ManagerRepoExt {

	ManagerEntity checkLogin(ManagerEntity manager);

	String convertMd5(String pass);

}
