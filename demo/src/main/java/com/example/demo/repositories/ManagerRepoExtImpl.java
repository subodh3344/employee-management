package com.example.demo.repositories;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.example.demo.model.ManagerEntity;


@Component
public class ManagerRepoExtImpl implements ManagerRepoExt{
	
	@Autowired
	private ManagerRepositories managerRepositories;

	@Override
	public ManagerEntity checkLogin(ManagerEntity manager){
		
		try {
			String email = manager.getEmail();
			String normalPass = manager.getPassword();
			String md5Pass = convertMd5(normalPass);
			Optional<ManagerEntity> gotManager = managerRepositories.checkManagerLogin(email, md5Pass);
			if(gotManager.isPresent()) {
				return gotManager.get();
			}
		}
		catch(Exception e) {
			System.out.println("Exception occured in checkLogin "+e.getMessage()+" caused by "+e.getCause());
			
		}
		return null;
	}

	@Override
	public String convertMd5(String passward) {
		if (passward == null || passward.length() == 0) {
			throw new IllegalArgumentException("Password Not Found");
		}
		StringBuffer passwordMD5 = new StringBuffer();
		try {
			MessageDigest md = MessageDigest.getInstance("MD5");
			md.update(passward.getBytes());
			byte[] hash = md.digest();
			for (int i = 0; i < hash.length; i++) {
				if ((0xff & hash[i]) < 0x10) {
					passwordMD5.append("0" + Integer.toHexString((0xFF & hash[i])));
				} else {
					passwordMD5.append(Integer.toHexString(0xFF & hash[i]));
				}
			}
		} catch (NoSuchAlgorithmException e) {
			System.out.println("Exception Occurred while Converting to MD5 : " + e.getStackTrace());
		}
		return passwordMD5.toString();
	}

}
