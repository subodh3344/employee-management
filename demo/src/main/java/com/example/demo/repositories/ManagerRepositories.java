package com.example.demo.repositories;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.example.demo.model.ManagerEntity;


public interface ManagerRepositories extends JpaRepository<ManagerEntity, Long>{
	

	@Query("SELECT m FROM ManagerEntity m where m.email= :email AND m.password= :pass ")
	Optional<ManagerEntity> checkManagerLogin(@Param("email") String email,@Param("pass") String pass);

	@Query("SELECT m FROM ManagerEntity m where m.email= :email")
	Optional<ManagerEntity> findByEmail(@Param("email") String email);

}
