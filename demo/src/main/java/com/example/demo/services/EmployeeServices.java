package com.example.demo.services;

import java.util.HashMap;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.model.EmployeeEntity;
import com.example.demo.model.ManagerEntity;
import com.example.demo.repositories.EmployeeRepositories;

@Service
public class EmployeeServices {
	
	@Autowired
	private EmployeeRepositories employeeRepositories;

	public List<EmployeeEntity> getAllEmployee() {
		try {
			List<EmployeeEntity> optEmps = employeeRepositories.findAll();
			return optEmps;
		}
		catch(Exception e) {
			System.out.println("Exception occured while get all employees "+e.getMessage()+" caused by "+e.getCause());
		}
		return null;
	}

	public ResponseEntity<Object> checkEmpIdIfExists(EmployeeEntity emp) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			Optional<EmployeeEntity> gotEmp = employeeRepositories.findByEmpId(emp.getEmp_id());
			if(gotEmp.isPresent()) {
				optObject = new ResponseEntity<Object>(null,HttpStatus.NOT_ACCEPTABLE);
			}
			else {
				optObject = new ResponseEntity<Object>(null,HttpStatus.OK);
			}
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

	public ResponseEntity<Object> createEmployee(EmployeeEntity emp) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			EmployeeEntity gotEmp = employeeRepositories.save(emp);
			optObject = new ResponseEntity<Object>(gotEmp,HttpStatus.OK);
			
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

	public ResponseEntity<Object> updateEmployee(EmployeeEntity emp) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			EmployeeEntity gotEmp = employeeRepositories.save(emp);
			optObject = new ResponseEntity<Object>(gotEmp,HttpStatus.OK);
			
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

	public ResponseEntity<Object> deleteEmpById(Long empId) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			employeeRepositories.deleteById(empId);
			HashMap<String, Boolean> opt = new HashMap<String, Boolean>();
			opt.put("Status", true);
			optObject = new ResponseEntity<Object>(opt,HttpStatus.OK);
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

}
