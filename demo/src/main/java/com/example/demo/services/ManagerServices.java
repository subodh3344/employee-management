package com.example.demo.services;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import com.example.demo.model.ManagerEntity;
import com.example.demo.repositories.ManagerRepoExt;
import com.example.demo.repositories.ManagerRepositories;

@Service
public class ManagerServices {

	@Autowired
	private ManagerRepositories managerRepositories;
	
	@Autowired
	private ManagerRepoExt managerRepoExt;
	
	public List<ManagerEntity> getAllManagers() {
		try {
			List<ManagerEntity> optmanagers = managerRepositories.findAll();
			return optmanagers;
		}
		catch(Exception e) {
			System.out.println("Exception occured while get all manager "+e.getMessage()+" caused by "+e.getCause());
		}
		return null;
	}

	public ResponseEntity<Object> checkLogin(ManagerEntity manager) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			ManagerEntity gotManager = managerRepoExt.checkLogin(manager);
			if(gotManager != null) {
				optObject = new ResponseEntity<Object>(gotManager,HttpStatus.OK);
			}
			else {
				optObject = new ResponseEntity<Object>(null,HttpStatus.UNAUTHORIZED);
			}
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

	public ResponseEntity<Object> createManager(ManagerEntity manager) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			manager.setPassword("827CCB0EEA8A706C4C34A16891F84E7B");
			ManagerEntity gotManager = managerRepositories.save(manager);
			optObject = new ResponseEntity<Object>(gotManager,HttpStatus.OK);
			
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

	public ResponseEntity<Object> checkEmailIfExists(ManagerEntity manager) {
		ResponseEntity<Object> optObject = new ResponseEntity<Object>(null,HttpStatus.INTERNAL_SERVER_ERROR);
		try {
			Optional<ManagerEntity> gotManager = managerRepositories.findByEmail(manager.getEmail());
			if(gotManager.isPresent()) {
				optObject = new ResponseEntity<Object>(null,HttpStatus.NOT_ACCEPTABLE);
			}
			else {
				optObject = new ResponseEntity<Object>(null,HttpStatus.OK);
			}
		}
		catch(Exception e) {
			
		}
		return optObject;
	}

}
