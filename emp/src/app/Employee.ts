export interface IEmployee{
    id : string, 
    emp_id : string, 
    first_name : string, 
    last_name : string, 
    address : string, 
    dob : string, 
    mobile : string, 
    city : string

}