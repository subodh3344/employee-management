import { Injectable } from '@angular/core';
import { MatDialog } from '@angular/material';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { EmployeeEditDialogComponent } from './employee-edit-dialog/employee-edit-dialog.component';

@Injectable({
  providedIn: 'root'
})
export class DialogService {

  constructor(private dialog:MatDialog) { }

  openDialog(title,msg){
    return this.dialog.open(ConfirmDialogComponent,{
      width :'390px',
      disableClose : true,
      panelClass : 'confirm-dialog-container',
      data : {
        title : title,
        message : msg
      }
    })
  }

  openEmployeeEditDialog(id,emp_id,first_name,last_name,address,dob,mobile,city){
    return this.dialog.open(EmployeeEditDialogComponent,{
      width :'80%',
      height : '80%',
      disableClose : true,
      panelClass : 'confirm-dialog-container',
      data : {
        id : id,
        emp_id : emp_id, 
        first_name : first_name, 
        last_name : last_name, 
        address : address, 
        dob : dob, 
        mobile : mobile, 
        city : city
      }
    });
  }
}
