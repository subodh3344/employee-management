import { Component, OnInit, Inject } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MAT_DIALOG_DATA } from '@angular/material';

@Component({
  selector: 'app-employee-edit-dialog',
  templateUrl: './employee-edit-dialog.component.html',
  styleUrls: ['./employee-edit-dialog.component.css']
})
export class EmployeeEditDialogComponent implements OnInit {

  employeeEditForm : FormGroup;

  constructor(@Inject(MAT_DIALOG_DATA) public data) { 
    this.employeeEditForm = new FormGroup({
      firstName : new FormControl(data.first_name,[Validators.required]),
      lastName : new FormControl(data.last_name,[Validators.required]),
      empId: new FormControl(data.emp_id,[Validators.required]),
      dob :new FormControl(data.dob,[Validators.required]),
      address: new FormControl(data.address,[Validators.required]),
      mobile : new FormControl(data.mobile,[Validators.required,Validators.min(1000000000), Validators.max(9999999999)]),
      city : new FormControl(data.city,[Validators.required])
    });
  }

  ngOnInit() {
  }

}
