/* 
Employee Service : 
It deals with all http calls for Manager and Employee */


import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { IManager } from './manager';
import { Observable } from 'rxjs';
import { IEmployee } from './Employee';

@Injectable({
  providedIn: 'root'
})
export class EmployeeService {
mainUrl : any;

httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/json',
  })
};
  constructor( private http : HttpClient) { 
    this.mainUrl = "http://localhost:8080/";
  }

  // Check login for manager
   checkLogin(emai:string,password:string){
     var input = '{"password": "'+password+'","email": "'+emai+'"}';
     return this.http.post(this.mainUrl+"/manager/checkLogin",input,this.httpOptions);
   }

   // Get all manager list
  getAllManagers() : Observable<IManager[]>{
    return this.http.get<IManager[]>(this.mainUrl+"manager/getAllManagers");
  }

  // check if email is already present in database (Manager)
  checkManagerEmail(email : string){
    var data = '{"email": "'+email+'"}';
    return this.http.post(this.mainUrl+"/manager/checkEmailIfExists",data,this.httpOptions);
    
  }

  // create new manager
  createManager(first_name : string, last_name : string, address : string, dob : string, company : string, email : string){
   var data = ' {"first_name": "'+first_name+'","last_name": "'+last_name+'","company": "'+company+'","address": "'+address+'","dob": "'+dob+'","email": "'+email+'"}';
  return this.http.post(this.mainUrl+"/manager/createManager",data,this.httpOptions);
  }

  // Fetch all employees
  getAllEmployees():Observable<IEmployee[]>{
    return this.http.get<IEmployee[]>(this.mainUrl+"employee/getAllEmployees");
  }

  // create new employee
  creatNewEmployee(emp_id : string, first_name : string, last_name :string, address : string, dob : string, mobile : string, city: string){
    var data = ' {"emp_id":"'+emp_id+'", "first_name":"'+first_name+'", "last_name":"'+last_name+'", "address":"'+address+'", "dob":"'+dob+'", "mobile":"'+mobile+'", "city":"'+city+'"}';
    return this.http.post(this.mainUrl+"/employee/createEmployee",data,this.httpOptions);
    
  }

  // Check if emp id is already exists
  checkEmpId(empId : string){
    var data = '{"emp_id":"'+empId+'"}';
    return this.http.post(this.mainUrl+"/employee/checkEmpIdIfExists",data,this.httpOptions);
  }

  // Delete employee
  deleteEmployee(empId : string){
    return this.http.get(this.mainUrl+"employee/deleteEmpById/"+empId);
  }

  updateEmployee(id : number , emp_id:string, first_name:string, last_name:string, address:string, dob:string, mobile:number, city:string){
    var updateEMpData = '{"id": '+id+',"emp_id": "'+emp_id+'","first_name": "'+first_name+'","last_name": "'+last_name+'", "address": "'+address+'","dob": "'+dob+'","mobile": "'+mobile+'","city": "'+city+'"}'
    return this.http.post(this.mainUrl+"/employee/updateEmployee",updateEMpData,this.httpOptions);
  }

}
