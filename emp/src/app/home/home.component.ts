import { Component, OnInit } from '@angular/core';
import {MatDialog,MatDialogConfig ,MatDialogRef, MAT_DIALOG_DATA} from '@angular/material/dialog';
import { ConfirmDialogComponent } from '../confirm-dialog/confirm-dialog.component';
import { DialogService } from '../dialog.service';
import { Router } from '@angular/router';
import { EmployeeService } from '../employee.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { MatSnackBar } from '@angular/material';
import * as moment from 'moment';
import { EmployeeEditDialogComponent } from '../employee-edit-dialog/employee-edit-dialog.component';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {

  displayedColumns: string[] = ['id', 'emp_id', 'first_name', 'last_name','address','dob','mobile','city','actions'];
  dataSource : any;
  showProgress : any = false;
  dialogRef : any;
  userName : any;
  empListView : any = true;
  employeeCreationForm : FormGroup;
  constructor(
    private dialogService : DialogService,
    private router : Router,
    private employeeService : EmployeeService,
    private _snackBar: MatSnackBar,
    private matDialog :MatDialog
    ) {

      this.employeeCreationForm = new FormGroup({
        firstName : new FormControl('',[Validators.required]),
        lastName : new FormControl('',[Validators.required]),
        empId: new FormControl('',[Validators.required]),
        dob :new FormControl('',[Validators.required]),
        address: new FormControl('',[Validators.required]),
        mobile : new FormControl('',[Validators.required,Validators.min(1000000000), Validators.max(9999999999)]),
        city : new FormControl('',[Validators.required])
      });
     }

    

  ngOnInit() {

    if(localStorage.getItem('isLoggedIn') != "true"){
      this.router.navigateByUrl("/login");
    }

    var name = localStorage.getItem('first_name') +"  "+ localStorage.getItem('last_name');
    this.userName = name;

    this.getEmpList();
  }

  logout(){
    this.showProgress = true;
    this.dialogService.openDialog("Logout","Are you sure ?")
    .afterClosed().subscribe(data=>{
      console.log("Data is "+data);
      if(data == true){
        localStorage.clear();
        this.router.navigateByUrl('/login');
        this.showProgress = false;
      }
      else{
        this.showProgress = false;
      }
    });
  }

  // Get all employees list
  getEmpList(){
    this.showProgress = true;
    this.employeeService.getAllEmployees().subscribe((data)=>{
      console.log("got employees "+data);
      this.dataSource = data;
      if(data.length == 0){
        this._snackBar.open("Employee list is empty.Please create empoyee.", "", {
          duration: 2000,
          panelClass: ['error-snackbar']
        });
       }
      
      this.showProgress = false;
    },
    error=>{
      console.log("Error occured while getting emloyees ."+error);
      this.showProgress = false;
    });
  }

// Create new employee hide/show form
  createNewEmployee(){
    this.empListView = !this.empListView;
  }

// Create new employee function
createNewEmployeeSubmit(){
  this.showProgress = true;
  var firstName  = this.employeeCreationForm.get('firstName').value;
  var lastName  = this.employeeCreationForm.get('lastName').value;
  var empId  = this.employeeCreationForm.get('empId').value;
  var dob  = this.employeeCreationForm.get('dob').value;
  var mobile  = this.employeeCreationForm.get('mobile').value;
  var address  = this.employeeCreationForm.get('address').value;
  var city  = this.employeeCreationForm.get('city').value;

  this.employeeService.checkEmpId(empId).subscribe((data)=>{
    this.showProgress = false;
    console.log("success "+JSON.stringify(data));
    // convert date to required format
    const newDate = new Date(dob);
    const formattedDate = moment(newDate).format("YYYY-MM-DD");
    this.employeeService.creatNewEmployee(empId,firstName,lastName,address,formattedDate,mobile,city).subscribe(data=>{
      this._snackBar.open("Employee created succesfully.", "", {
        duration: 2000,
        panelClass: ['success-snackbar']
      });
      this.empListView = !this.empListView;
      this.getEmpList();
    },
    (error)=>{
      this._snackBar.open("Error occured "+JSON.stringify(error), "", {
        duration: 2000,
        panelClass: ['error-snackbar']
      });
      this.showProgress = false;
    })
  },
  (error)=>{
    console.log("eroor "+error);
     if(error.status == 406){
      this._snackBar.open("Employee Id is already present.", "", {
        duration: 2000,
        panelClass: ['error-snackbar']
      });
     }
     this.showProgress = false;
  });
}
editEmp(emp){
    console.log("Edit "+emp['id']);
    this.dialogService.openEmployeeEditDialog(emp['id'],emp['emp_id'],emp['first_name'],emp['last_name'],emp['address'],emp['dob'],emp['mobile'],emp['city'])
    .afterClosed().subscribe(data=>{
      // Check if user pressed cancel button from dialog box
      if(data != false){
        this.showProgress = true;
      // convert date to required format
      const newDate = new Date(data['dob']);
      const formattedDate = moment(newDate).format("YYYY-MM-DD");
      console.log("data is "+JSON.stringify(data)+" NAME "+data['firstName']);
      this.showProgress = false;
      this.employeeService.updateEmployee(emp['id'],data['empId'],data['firstName'],data['lastName'],data['address'],formattedDate,data['mobile'],data['city']).subscribe(data=>{
        this._snackBar.open("Employee updated succesfully.", "", {
          duration: 2000,
          panelClass: ['success-snackbar']
        });
        this.getEmpList();
      },
      error=>{
        this._snackBar.open("Error occured while employee updation.", "", {
          duration: 2000,
          panelClass: ['error-snackbar']
        });
      }); 
      }
      
    });
  }

  deleteEmp(emp){
    console.log("delete "+emp);
    this.showProgress = true;
    this.dialogService.openDialog("Delete","Are you sure ?")
    .afterClosed().subscribe(data=>{
      console.log("Data is "+data);
      if(data == true){
        this.employeeService.deleteEmployee(emp['id']).subscribe((data)=>{
          this._snackBar.open("Employee deleted succesfully.", "", {
            duration: 2000,
            panelClass: ['success-snackbar']
          });
          this.getEmpList();
        })
      }
      else{
        this.showProgress = false;
      }
    });
  }

}

