import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl ,Validators} from '@angular/forms';
import { EmployeeService } from '../employee.service';
import { MatSnackBar } from '@angular/material/snack-bar'; 
import { Router } from '@angular/router';
import * as moment from 'moment';


@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  managerRegistrationForm : FormGroup;
  managerLoginForm : FormGroup;
  gender:any;
  regForm : any = false;
  hide = true;
  constructor(
    private empServices : EmployeeService,
    private _snackBar: MatSnackBar,
    private router : Router
    ) { }

  ngOnInit() {

    // Registration form
    this.managerRegistrationForm = new FormGroup({
      firstName : new FormControl('',[Validators.required]),
      lastName : new FormControl('',[Validators.required]),
      address: new FormControl('',[Validators.required]),
      dob :new FormControl('',[Validators.required]),
      company: new FormControl('',[Validators.required]),
      email : new FormControl('',[Validators.email,Validators.required])
    });

    // Login form with validations
    this.managerLoginForm = new FormGroup({
      loginEmail : new FormControl('',[Validators.required,Validators.email]),
      password : new FormControl('',[Validators.required])
    });

    // Testing
    this.empServices.getAllManagers().subscribe((data)=>{
      console.log(data);
    });


  }


  // Login functionality
  login(){
    console.log("Values are "+JSON.stringify(this.managerLoginForm.value));
    var email  = this.managerLoginForm.get('loginEmail').value;
    var password = this.managerLoginForm.get("password").value;
    this.empServices.checkLogin(email,password).subscribe((manager)=>{
        console.log("logged in manager "+JSON.stringify(manager));
       
       // Store data for future usage
        localStorage.setItem('id',manager['id']);
        localStorage.setItem('first_name',manager['first_name']);
        localStorage.setItem('last_name',manager['last_name']);
        localStorage.setItem('address',manager['address']);
        localStorage.setItem('dob',manager['dob']);
        localStorage.setItem('company',manager['company']);
        localStorage.setItem('email',manager['email']);
        localStorage.setItem('isLoggedIn',"true");
        this.router.navigateByUrl("/home");
    },
    (error)=>{
     console.log("Error in login "+JSON.stringify(error)+" status "+error.status);
     if(error.status == 401){
      this._snackBar.open("Please check credentials", "ok", {
        duration: 2000,
        panelClass: ['error-snackbar']
      });
     } 
    });
  }

  // Registration functionality
  register(){
    var first_name = this.managerRegistrationForm.get('firstName').value;
    var last_name = this.managerRegistrationForm.get('lastName').value;
    var address = this.managerRegistrationForm.get('address').value;
    
    var dob = this.managerRegistrationForm.get('dob').value;
    // convert date to required format
    const newDate = new Date(dob);
    const formattedDate = moment(newDate).format("YYYY-MM-DD");

    var company = this.managerRegistrationForm.get('company').value;
    var email = this.managerRegistrationForm.get('email').value;
    // Check if email id is already present in database
    this.empServices.checkManagerEmail(email).subscribe((opt)=>{
      console.log("Check manager email opt "+opt);
      this.empServices.createManager(first_name,last_name,address,formattedDate,company,email).subscribe((opt)=>{
          console.log("manager creation opt "+JSON.stringify(opt));
          this._snackBar.open("New manager created.Please use following credentials for login email :"+email+" , Password : 12345", "ok", {
            duration: 5000,
            panelClass: ['success-snackbar']
          });

          // Hide register form & reset fields
          this.regForm = false;
          this.managerRegistrationForm.reset();
      },
      (error)=>{
        this._snackBar.open("Error occured while manager creation."+JSON.stringify(error), "ok", {
          duration: 5000,
          panelClass: ['error-snackbar']
        });
      })
    },
    (error)=>{
      console.log("Check email exists error "+error);
      // Email already exists
      if(error.status == 406){
        this._snackBar.open("Email already exists,Please try another email.", "ok", {
          duration: 5000,
          panelClass: ['error-snackbar']
        });
      }
    })
  }

}
